{{/*
Expand the name of the chart.
*/}}
{{- define "xsugar.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "xsugar.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "xsugar.labels" -}}
helm.sh/chart: {{ include "xsugar.chart" . }}
{{ include "xsugar.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "xsugar.selectorLabels" -}}
app.kubernetes.io/name: {{ include "xsugar.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}
